import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from './../../actions/chatActions'
import fetchMock from 'fetch-mock'
import expect from 'expect' // You can use any testing library

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('chat actions', () => {
    afterEach(() => {
        fetchMock.restore()
    })

    it('GET_ALL_CONVERSATIONS success', () => {
        const mockConv = [{
            conversationId: 1,
            idService: 333,
            lastMessageTimeStamp: new Date("2019-04-06"),
            messages: [{
                idMessage: 1,
                idUserProfileSender: 6008,
                idConversation: 1,
                dateAndTimeSent: new Date("2019-04-06"),
                messageText: "Hello Steve!"
            }, {
                idMessage: 2,
                idUserProfileSender: 7004,
                idConversation: 1,
                dateAndTimeSent: new Date("2019-04-06"),
                messageText: "Hello Bob!"
            }],
            serviceDetail: {
                id: 333,
                idProvider: 934,
                title: 'Gardening services',
                category: 'Gardening',
                price: '405',
                priceUnit: '$',
                user: {
                    id: 934,
                    name: 'Bobber'
                }
            }
        }, {
            conversationId: 2,
            idService: 444,
            lastMessageTimeStamp: new Date("2019-03-20"),
            messages: [{
                idMessage: 1,
                idUserProfileSender: 3124,
                idConversation: 2,
                dateAndTimeSent: new Date("2019-03-20"),
                messageText: "Hello World!"
            }, {
                idMessage: 2,
                idUserProfileSender: 4413,
                idConversation: 2,
                dateAndTimeSent: new Date("2019-03-20"),
                messageText: "Hello Dlrow!"
            }],
            serviceDetail: {
                id: 444,
                idProvider: 756,
                title: 'Cleaning services',
                category: 'Cleaning',
                price: '122',
                priceUnit: '$',
                user: {
                    id: 756,
                    name: 'Bob'
                }
            }
        }]

        // fetchMock.get('/conversations', {
        //     data: {
        //         conversations: mockConv
        //     },
        //     headers: {
        //         'content-type': 'application/json'
        //     }
        // })

        const expectedActions = [{
            type: "GET_ALL_CONVERSATIONS",
            payload: mockConv
        }]

        const store = mockStore({
            conversations: []
        })

        return store.dispatch(actions.getAllConversations()).then(() => {
            // return of async actions
            expect(store.getActions()).toEqual(expectedActions)
        })
    })
})