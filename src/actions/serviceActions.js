import {
    getServiceById,
    getUserById
} from './../services/helpOutApi'

function loadDetail(serviceDetail) {
    return {
        type: "LOAD_SERVICE_DETAIL",
        payload: serviceDetail
    }
}

export function loadServiceDetail(serviceId) {
    return dispatch => {
        return getServiceById(serviceId).then((service) => {
            getUserById(service.idProvider).then((user) => {
                dispatch(loadDetail({
                    ...service,
                    user: user
                }))
            })
        })
    }
}