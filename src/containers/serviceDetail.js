import ServiceDetail from './../components/Services/ServiceDetail'
import {loadServiceDetail} from './../actions/serviceActions'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

function mapStateToProps(state) {
	return {
		serviceDetailReducer: state.serviceDetailReducer
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		loadServiceDetail: loadServiceDetail
	}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ServiceDetail)