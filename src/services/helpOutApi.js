// mock database
Date.prototype.substractDays = function(days) {
    this.setDate(this.getDate() - parseInt(days));
    return this;
};

let oneDayBack = new Date()
oneDayBack.substractDays(1)

let twoDaysBack = new Date()
twoDaysBack.substractDays(2)

const mockConv = [{
    conversationId: 1,
    idService: 333,
    lastMessageTimeStamp: new Date("2019-04-06"),
    messages: [{
        idMessage: 1,
        idUserProfileSender: 6008,
        idConversation: 1,
        dateAndTimeSent: new Date("2019-04-06"),
        messageText: "Hello Steve!"
    }, {
        idMessage: 2,
        idUserProfileSender: 7004,
        idConversation: 1,
        dateAndTimeSent: new Date("2019-04-06"),
        messageText: "Hello Bob!"
    }]
}, {
    conversationId: 2,
    idService: 444,
    lastMessageTimeStamp: new Date("2019-03-20"),
    messages: [{
        idMessage: 1,
        idUserProfileSender: 3124,
        idConversation: 2,
        dateAndTimeSent: new Date("2019-03-20"),
        messageText: "Hello World!"
    }, {
        idMessage: 2,
        idUserProfileSender: 4413,
        idConversation: 2,
        dateAndTimeSent: new Date("2019-03-20"),
        messageText: "Hello Dlrow!"
    }]
}]

const mockServices = [{
    id: 333,
    idProvider: 934,
    title: "Gardening services",
    category: "Gardening",
    price: "405",
    priceUnit: "$"
}, {
    id: 444,
    idProvider: 756,
    title: "Cleaning services",
    category: "Cleaning",
    price: "122",
    priceUnit: "$"
}, {
    id: 888,
    idProvider: 500,
    title: "Electrical services",
    category: "Electrical repairs",
    price: "50",
    priceUnit: "$"
}]

const mockUsers = [{
    id: 756,
    name: "Bob"
}, {
    id: 934,
    name: "Bobber"
}, {
    id: 500,
    name: "Bobescus"
}]

// API------------------------------------------

// api call to AddConversation
export function addConversation(conversation) {

    return new Promise(function(resolve, reject) {

        mockConv.push(conversation)
        resolve()
    })

}

// api call to GetConversations
export function getConversations() {

    return new Promise(function(resolve, reject) {

        let combinedConversations = []

        mockConv.forEach(c => {
            const service = mockServices.find(s => s.id === c.idService)
            const user = mockUsers.find(u => u.id === service.idProvider)
            const fullConv = {
                ...c,
                serviceDetail: {
                    ...service,
                    user: user
                }
            }
            combinedConversations.push(fullConv)
        })

        combinedConversations.sort((a, b) => {
            return b.LastMessageTimeStamp - a.LastMessageTimeStamp
        })

        resolve(combinedConversations)

    })
}

// call to GetServiceById
export function getServiceById(serviceId) {

    return new Promise(function(resolve, reject) {

        const service = mockServices.find(s => s.id === serviceId)
        resolve(service)

    })

}

// call to GetUserById
export function getUserById(userId) {

    return new Promise(function(resolve, reject) {

        const user = mockUsers.find(u => u.id === userId)
        resolve(user)

    })

}

// call to AddMessageToConv
export function addMessageToConv(msg) {

	return new Promise(function(resolve, reject) {
		mockConv.forEach(c => {
			if(c.conversationId === msg.idConversation) {
				c.messages.push(msg)
			}
		})
		resolve()
	})
}