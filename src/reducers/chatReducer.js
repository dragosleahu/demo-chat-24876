const initialState = {
	conversations: [],
	selectedConvIdService: -1
}

function chatReducer(state = initialState, action) {
	switch(action.type) {
		case "GET_ALL_CONVERSATIONS":
			return {
				conversations: action.payload,
				selectedConvIdService: state.selectedConvIdService
			}
		case "GET_SELECTED_CONVERSATION":
			return {
				conversations: state.conversations,
				selectedConvIdService: action.payload
			}
		default:
			return state
	}
}

export default chatReducer