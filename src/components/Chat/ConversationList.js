import React from 'react';

const ConversationList = ({conversations, onClickConversation}) => {
	return (
	  <div className="ConversationList">
	  	{conversations && conversations.length > 0 ? conversations.map((c, i) => {
	  		return <li key={i} onClick={() => onClickConversation(c.idService)}>{c.idService + " - " + c.serviceDetail.title}</li>
	  	}): "No conversations at the moment..."}
	  </div>
	);
}

export default ConversationList;
