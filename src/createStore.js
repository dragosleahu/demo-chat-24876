import {createStore, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import allReducers from './reducers'

export default function() {
	return createStore(allReducers, applyMiddleware(thunk))
}
