const initialState = {
	serviceDetail: {}
}

function serviceDetailReducer(state = initialState, action) {
	switch(action.type) {
		case "LOAD_SERVICE_DETAIL":
			return {
				...state,
				serviceDetail: action.payload
			}
		default:
			return state
	}
}

export default serviceDetailReducer