import React from 'react';
import {Redirect} from 'react-router-dom'

class ServiceDetail extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			redirectToMessage: false
		}

		this.routeChange = this.routeChange.bind(this)
	}

	componentWillMount() {
		this.props.loadServiceDetail(888)
	}

	routeChange() {
        this.setState({
        	redirectToMessage: true
        })
  	}

	render() {
		if(this.state.redirectToMessage) {
			return (
			<Redirect to={{
	        	pathname: '/chat',
	        	state: { newConversation: true, serviceDetail: this.props.serviceDetailReducer.serviceDetail }
			}}/>)
		} else {
			return (
			<div className="ServiceDetail">
			  	{this.props.serviceDetailReducer.serviceDetail && this.props.serviceDetailReducer.serviceDetail.title ? this.props.serviceDetailReducer.serviceDetail.title : "No service detail..."}
			  	<br/>
			  	<hr/>
			  	<button type="button" onClick={this.routeChange}>Message</button>
			</div>)
		}
	}
}

export default ServiceDetail;
