import Chat from './../components/Chat/Chat'
import {getAllConversations, setSelectedConversation, addNewConversation, addMessageToConversation} from './../actions/chatActions'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

function selectConversation(conversations, selectedConvIdService) {
	return conversations.find(c => c.idService === selectedConvIdService)
}

function mapStateToProps(state) {
	return {
		chatReducer: state.chatReducer,
		selectedConversation: selectConversation(state.chatReducer.conversations, state.chatReducer.selectedConvIdService)
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		getAllConversations: getAllConversations,
		setSelectedConversation: setSelectedConversation,
		addNewConversation: addNewConversation,
		addMessageToConversation: addMessageToConversation
	}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat)