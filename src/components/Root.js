import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import {Provider} from 'react-redux';
import App from './App/App'
import Header from './SharedLayout/Header'
import Chat from './../containers/chat'
import ServiceDetail from './../containers/serviceDetail'

const Root = ({ store }) => (
  <Provider store={store}>
        <Router>
          <div>
            <Header />
            <Switch>
              <Route exact path="/" component={App} />
              <Route path="/chat" component={Chat} />
              <Route path="/service-detail" component={ServiceDetail} />
            </Switch>
          </div>
        </Router>
  </Provider>
)

export default Root