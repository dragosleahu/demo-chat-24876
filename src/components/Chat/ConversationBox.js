import React, { Component } from 'react';

const ConversationBox = ({conversation}) => {
	return (
      <div className="ConversationBox">
      	{conversation && conversation.messages && conversation.messages.length > 0 ? conversation.messages.map(m => {
      		return <li key={m.idMessage}>{m.messageText}</li>
      	}) : "No messages at the moment..."}
      </div>
    );
}

export default ConversationBox;
