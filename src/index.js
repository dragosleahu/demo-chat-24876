import React from 'react';
import ReactDOM from 'react-dom';
import Root from './components/Root'
import 'bootstrap/dist/css/bootstrap.min.css';
import createStore from './createStore'

const store = createStore()

ReactDOM.render(<Root store={store}/>, document.getElementById('root'));
