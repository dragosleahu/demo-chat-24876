import React, {
    Component
} from 'react';

class ConversationInput extends Component {

    constructor(props) {
        super(props)

        this.state = {
            messageInput: ''
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(e) {
        e.preventDefault()

        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit() {
    	if(this.props.conversation) {
    		const msg = {
	        	idReceiver: this.props.conversation.idReceiver,
	        	idConversation: this.props.conversation.idConversation,
	            dateTimeSent: new Date(),
	            messageText: this.state.messageInput
        	};

        	this.props.addMessageToConversation(msg)
    	}
    	this.setState({
    		messageInput: ''
    	})
    }

    render() {
        return ( <
            div className = "ConversationInput" >
            <
            input type = "text"
            name = "messageInput"
            value = {
                this.state.messageInput
            }
            onChange = {
                this.handleChange
            }
            />  <
            button type = "submit"
            onClick = {
                this.handleSubmit
            } > Send < /button>  < /
            div >
        );
    }
}

export default ConversationInput;