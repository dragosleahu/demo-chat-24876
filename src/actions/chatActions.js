import {
    addConversation,
    getConversations,
    addMessageToConv
} from './../services/helpOutApi'

function allConversations(conversations) {
    return {
        type: "GET_ALL_CONVERSATIONS",
        payload: conversations
    }
}

export function getAllConversations() {
    return dispatch => {
        return getConversations().then((conversations) => {
            dispatch(allConversations(conversations))
        })
    }
}

function selectConversation(selectedConvIdService) {
    return {
        type: "GET_SELECTED_CONVERSATION",
        payload: selectedConvIdService
    }
}

export function setSelectedConversation(selectedConvIdService) {
    return dispatch => {
        return new Promise(function(resolve, reject) {
            dispatch(selectConversation(selectedConvIdService))
            resolve()
        })
    }
}

export function addNewConversation(conversationDetails) {
    return dispatch => {
        return addConversation(conversationDetails)
    }
}

export function addMessageToConversation(msg) {
    return dispatch => {
        return addMessageToConv(msg).then(() => {

            getConversations().then((conversations) => {

                dispatch(allConversations(conversations))

            })

        })
    }
}