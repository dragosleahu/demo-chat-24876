import React, { Component } from 'react';
import './Chat.css'
import ConversationList from './ConversationList'
import ConversationDetail from './ConversationDetail'
import ConversationBox from './ConversationBox'
import ConversationInput from './ConversationInput'

class Chat extends Component {

constructor(props) {
	super(props)

	this.state = {
		currentOpenedConversation: {}
	}

	this.handleConversationClick = this.handleConversationClick.bind(this)
}

  componentDidMount() {
	if(this.props.location.state && this.props.location.state.newConversation &&
		this.props.location.state.serviceDetail) { // if accessing chat from service detail
		const newConversation = {
			idService: this.props.location.state.serviceDetail.id,
			idProvider: this.props.location.state.serviceDetail.user.id,
			providerName: this.props.location.state.serviceDetail.user.name,
			lastMessageTimeStamp: new Date(),
			messages: []
		}
		this.props.addNewConversation(newConversation).then(() => {
			this.props.getAllConversations().then(() => {
				this.selectConversation(newConversation.idService)
			})
		})
	} else { // if accessing chat from navbar
		this.props.getAllConversations().then(() => {
			this.selectConversation(this.props.chatReducer.conversations[0].idService)
		})
	}
  }

  handleConversationClick(selectedConvIdService) {
  	this.selectConversation(selectedConvIdService)
  }

  selectConversation(selectedConvIdService) {
  	  	this.props.setSelectedConversation(selectedConvIdService).then(() => {
  			this.setCurrentOpenedConversation(this.props.selectedConversation)
  	})
  }

  setCurrentOpenedConversation(selectedConversation) {
	this.setState({
		currentOpenedConversation: selectedConversation
	})
  }

  render () {
    return (
      <div className="Chat">
      	<div className="leftPane">
			<ConversationList conversations={this.props.chatReducer.conversations} onClickConversation={this.handleConversationClick}/>
      	</div>
      	<div className="rightPane">
      		<ConversationDetail conversationDetail={this.state.currentOpenedConversation.serviceDetail}/>
      		<ConversationBox conversation={this.state.currentOpenedConversation}/>
      		<ConversationInput conversation={this.state.currentOpenedConversation} addMessageToConversation={this.props.addMessageToConversation}/>
      	</div>
      </div>
    );
  }
}

export default Chat;