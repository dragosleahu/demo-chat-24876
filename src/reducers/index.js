import {combineReducers} from 'redux'
import chatReducer from './chatReducer'
import serviceDetailReducer from './serviceDetailReducer'

export default combineReducers({
	chatReducer,
	serviceDetailReducer
})