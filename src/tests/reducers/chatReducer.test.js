import reducer from './../../reducers/chatReducer'
import expect from 'expect' // You can use any testing library

describe('chat reducer', () => {
    const mockConv = [{
        conversationId: 1,
        idService: 333,
        lastMessageTimeStamp: new Date("2019-04-06"),
        messages: [{
            idMessage: 1,
            idUserProfileSender: 6008,
            idConversation: 1,
            dateAndTimeSent: new Date("2019-04-06"),
            messageText: "Hello Steve!"
        }, {
            idMessage: 2,
            idUserProfileSender: 7004,
            idConversation: 1,
            dateAndTimeSent: new Date("2019-04-06"),
            messageText: "Hello Bob!"
        }],
        serviceDetail: {
            id: 333,
            idProvider: 934,
            title: 'Gardening services',
            category: 'Gardening',
            price: '405',
            priceUnit: '$',
            user: {
                id: 934,
                name: 'Bobber'
            }
        }
    }, {
        conversationId: 2,
        idService: 444,
        lastMessageTimeStamp: new Date("2019-03-20"),
        messages: [{
            idMessage: 1,
            idUserProfileSender: 3124,
            idConversation: 2,
            dateAndTimeSent: new Date("2019-03-20"),
            messageText: "Hello World!"
        }, {
            idMessage: 2,
            idUserProfileSender: 4413,
            idConversation: 2,
            dateAndTimeSent: new Date("2019-03-20"),
            messageText: "Hello Dlrow!"
        }],
        serviceDetail: {
            id: 444,
            idProvider: 756,
            title: 'Cleaning services',
            category: 'Cleaning',
            price: '122',
            priceUnit: '$',
            user: {
                id: 756,
                name: 'Bob'
            }
        }
    }]

    const selectedConvId = 1

    it('should return the initial state', () => {

        expect(reducer(undefined, {
            conversations: [],
            selectedConvIdService: -1
        })).toEqual({
            conversations: [],
            selectedConvIdService: -1
        })

    })

    it('should handle GET_ALL_CONVERSATIONS', () => {

        expect(
            reducer({
                conversations: [],
                selectedConvIdService: -1
            }, {
                type: "GET_ALL_CONVERSATIONS",
                payload: mockConv
            })
        ).toEqual({
            conversations: mockConv,
            selectedConvIdService: -1
        })

    })

    it('should handle GET_SELECTED_CONVERSATION', () => {
    	
        expect(
            reducer({
                conversations: mockConv,
                selectedConvIdService: -1
            }, {
                type: "GET_SELECTED_CONVERSATION",
                payload: selectedConvId
            })
        ).toEqual({
            conversations: mockConv,
            selectedConvIdService: 1
        })

    })

})