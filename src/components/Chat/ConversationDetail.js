import React, { Component } from 'react';

const ConversationDetail = ({conversationDetail}) => {
	return (
      <div className="ConversationDetail">
      	{conversationDetail && Object.entries(conversationDetail).length > 0 ? conversationDetail.title : "No conversation detail..."}
      </div>
    );
}

export default ConversationDetail;
